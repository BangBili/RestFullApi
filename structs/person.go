package structs

import (
	"github.com/jinzhu/gorm"
)

type Person struct {
	gorm.Model
	Username    string `json:"username" validate:"required"`
	Password    string `json:"password" validate:"required"`
	NamaLengkap string `json:"namaLengkap" validate:"required"`
	Poto        string `json:"poto" validate:"required"`
}
