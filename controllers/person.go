package controllers

import (
	"fmt"
	"net/http"

	"../structs"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator"
)

var validate *validator.Validate

// to get one data with {id}
func (idb *InDB) GetPerson(c *gin.Context) {
	var (
		person structs.Person
		result gin.H
	)
	id := c.Param("id")
	err := idb.DB.Where("id = ?", id).First(&person).Error
	if err != nil {
		result = gin.H{
			"result": err.Error(),
			"count":  0,
		}
	} else {
		result = gin.H{
			"result": person,
			"count":  1,
		}
	}

	c.JSON(http.StatusOK, result)
}

// get all data in person
func (idb *InDB) GetPersons(c *gin.Context) {
	var (
		persons []structs.Person
		result  gin.H
	)

	idb.DB.Find(&persons)
	if len(persons) <= 0 {
		result = gin.H{
			"result": nil,
			"count":  0,
		}
	} else {
		result = gin.H{
			"result": persons,
			"count":  len(persons),
		}
	}

	c.JSON(http.StatusOK, result)
}

// create new data to the database
func (idb *InDB) CreatePerson(c *gin.Context) {
	var (
		person structs.Person
		result gin.H
	)
	username := c.PostForm("username")
	password := c.PostForm("password")
	namaLengkap := c.PostForm("namaLengkap")
	poto, err := c.FormFile("poto")

	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("err: %s", err.Error()))
		return
	}
	if username == "" {
		c.String(http.StatusBadRequest, fmt.Sprintf("err:Username belum diisi"))
		return
	}
	if password == "" {
		c.String(http.StatusBadRequest, fmt.Sprintf("err: Password tidak boleh kosong"))
		return
	}
	if namaLengkap == "" {
		c.String(http.StatusBadRequest, fmt.Sprintf("err: Nama Lengkap belum diisi"))
		return
	}

	path := "images/" + poto.Filename
	if err := c.SaveUploadedFile(poto, path); err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("err: %s", err.Error()))
		return
	}

	person.Username = username
	person.Password = password
	person.NamaLengkap = namaLengkap
	person.Poto = path

	idb.DB.Create(&person)
	result = gin.H{
		"result": person,
	}
	c.JSON(http.StatusOK, result)
}

// update data with {id} as query
func (idb *InDB) UpdatePerson(c *gin.Context) {
	id := c.Query("id")
	username := c.PostForm("username")
	password := c.PostForm("password")
	namaLengkap := c.PostForm("namaLengkap")
	poto := c.PostForm("poto")
	var (
		person    structs.Person
		newPerson structs.Person
		result    gin.H
	)

	err := idb.DB.First(&person, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	newPerson.Username = username
	newPerson.Password = password
	newPerson.NamaLengkap = namaLengkap
	newPerson.Poto = poto
	err = idb.DB.Model(&person).Updates(newPerson).Error
	if err != nil {
		result = gin.H{
			"result": "update failed",
		}
	} else {
		result = gin.H{
			"result": "successfully updated data",
		}
	}
	c.JSON(http.StatusOK, result)
}

// delete data with {id}
func (idb *InDB) DeletePerson(c *gin.Context) {
	var (
		person structs.Person
		result gin.H
	)
	id := c.Param("id")
	err := idb.DB.First(&person, id).Error
	if err != nil {
		result = gin.H{
			"result": "data not found",
		}
	}
	err = idb.DB.Delete(&person).Error
	if err != nil {
		result = gin.H{
			"result": "delete failed",
		}
	} else {
		result = gin.H{
			"result": "Data deleted successfully",
		}
	}

	c.JSON(http.StatusOK, result)
}

func (idb *InDB) LoginHandler(c *gin.Context) {
	var (
		user structs.Person
	)
	username := c.PostForm("username")
	password := c.PostForm("password")
	err := idb.DB.Where("username = ?", username).First(&user).Error

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"status":  http.StatusBadRequest,
			"message": "can't bind struct",
		})
	}
	if user.Password != password {
		c.JSON(http.StatusUnauthorized, gin.H{
			"status":  http.StatusUnauthorized,
			"message": "wrong username or password",
		})
	}
	sign := jwt.New(jwt.GetSigningMethod("HS256"))
	token, err := sign.SignedString([]byte("secret"))
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"message": err.Error(),
		})
		c.Abort()
	}
	c.JSON(http.StatusOK, gin.H{
		"user":  user,
		"token": token,
	})
}
